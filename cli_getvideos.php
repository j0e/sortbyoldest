<?php
if (PHP_SAPI !== 'cli') {
	http_response_code(404);
	exit;
}

// usage: php cli_getvideos.php <channel_id>
// TODO maybe only need uploads_id? then can infer channel_id from first api result
//      although possibility to pass random playlists in...
//      better just pass channel id, one extra api call is worth reassurance that we're saving correct data
//      (channel name, num. videos, etc)

if (!isset($argv[1])) {
	echo "usage: $argv[0] <channel_id>\n"; // TODO echo to stderr
	exit(0);
}

$channel_id = trim($argv[1]);

if (!preg_match('/UC[a-zA-Z0-9_-]{21}[AQgw]/', $argv[1])) {
	echo "channel ID is in incorrect format\n";
	echo "usage: $argv[0] <channel_id>\n";
	exit(1);
}

require_once(__DIR__.'/config.php');




