<?php defined('SBO_EXE') or die;

// sortbyoldest example config file (pseudoish-cPanel-style dir structure shown, may differ on other platforms)

// the base URL of the website, relative to the web root directory
define('HTTP_SERVER', 'https://sortbyoldest.j0e.uk/');

// the absolute path of the web root directory
define('DIR_WEBROOT',  '/home/your_username/public_html/');

// absolute path of cache directory (optional, defaults to "${DIR_WEBROOT}cache")
define('DIR_CACHE', '/home/your_username/sortbyoldest_cache/');

// absolute path of log files directory (optional, defaults to "${DIR_WEBROOT}cache")
define('DIR_LOGS', '/home/your_username/sortbyoldest_logs/');

// it is recommended to have your api key file in a separate folder outside the web root
require_once('../private/config_apikey.php'); 

// any other PHP INI settings you may wish to have
ini_set('max_execution_time', 600);



