<?php
define('SBO_EXE', 1);

// config //
require_once(__DIR__ . '/config.php');

// get the stuff we need // 'src' and 'vendor' folders from: https://github.com/googleapis/google-api-php-client/releases
require_once('vendor/autoload.php');

// defaults //
// TODO defaults for HTTP_SERVER and DIR_ROOT from LSE

if (!defined('PAGE_NUMVIDEOS')) {
	define('PAGE_NUMVIDEOS', 48);
}

if (!defined('DOMAIN')) {
	define('DOMAIN', trim(preg_replace('~^https?://~', '', HTTP_SERVER), '/'));
}

// may be needed if your server is slow and keeps timing out on channels with lots of vids
// ini_set('max_execution_time', 600);

// function defs ///////////////////////////////////////////////////////////////
use function htmlspecialchars as H; // saves typing :^)

/* cache */
if (!defined('CACHE_EXPIRE')) {
	define('CACHE_EXPIRE', 60*60*24 * 7); // roughly one week
}

if (!defined('DIR_CACHE')) {
	define('DIR_CACHE', DIR_WEBROOT . 'cache/');
}
if (!is_dir(DIR_CACHE)) {
	mkdir(DIR_CACHE, 0755);
}

function cache_read($key) {
	$files = glob(DIR_CACHE . preg_replace('/[^a-zA-Z0-9\._-]/', '', $key) . '.*');

	if ($files) {
		$file = fopen($files[0], 'r');
		flock($file, LOCK_SH);

		$data = fread($file, filesize($files[0]));

		flock($file, LOCK_UN);
		fclose($file);

		return json_decode($data, true);
	}

	return null;
}

function cache_write($key, $value, $expire=CACHE_EXPIRE) {
	foreach (glob(DIR_CACHE . preg_replace('/[^a-zA-Z0-9\._-]/', '', $key) . '.*') as $file) {
		if (is_file($file)) {
			unlink($file);
		}
	}

	$filename = DIR_CACHE . preg_replace('/[^a-zA-Z0-9\._-]/', '', $key) . '.' . (time()+$expire);

	$file = fopen($filename, 'w');
	flock($file, LOCK_EX);

	fwrite($file, json_encode($value));
	fflush($file);

	flock($file, LOCK_UN);
	fclose($file);
}

/* log */
if (!defined('DIR_LOGS')) {
	define('DIR_LOGS', DIR_WEBROOT . 'logs/');
}
if (!is_dir(DIR_LOGS)) {
	mkdir(DIR_LOGS, 0755);
}

class Log {
	private $handle;
	
	public function __construct($filename) {
		$this->handle = fopen(DIR_LOGS . $filename, 'a');
	}

	public function write($message) {
		fwrite($this->handle, date('Y-m-d H:i:s') . ' - ' . print_r($message, true) . "\n");
	}

	public function __destruct() {
		fclose($this->handle);
	}
}
$log = new Log('sortbyoldest.log');

/* api */
if (!defined('API_MAXRESULTS')) {
	define('API_MAXRESULTS', 50); // max number of vids yt allows per API call :(
}

function api_init() {
	static $api;

	if (!($api instanceof Google\Service\YouTube)) {
		try {
			$client = new Google\Client();
			$client->setApplicationName('sortbyoldest');
			$client->setDeveloperKey(APIKEY);
			$api = new Google\Service\YouTube($client);
		} catch (Exception $e) {
			throw new Exception('API initialisation failed: '.$e->getMessage());
		}
	}

	return $api;
}

function api_get_channel_id_by_username($username) {
	$channel_id = cache_read('username.' . $username);

	if (!isset($channel_id)) {
		$api = api_init();

		try {
			$channels = $api->channels->listChannels('contentDetails', ['forUsername' => $username]);

			$channel_id = $channels['items'][0]['id'] ?? false;

			cache_write('username.' . $username, $channel_id, CACHE_EXPIRE);
		} catch (Exception $e) {
			throw new Exception('API error when retrieving channel ID of username `'.H($username).'`: '.H($e->getMessage()).'`');
		}
	}

	if ($channel_id === false) {
		throw new Exception('No channel found that matches username `'.H($username).'`!');
	}

	return $channel_id;
}

function api_get_channel($channel_id) {
	$channel = cache_read('channel.' . $channel_id);

	if (!isset($channel)) {
		$api = api_init();

		try {
			$channels = $api->channels->listChannels('contentDetails,snippet', ['id' => $channel_id]);
		} catch (Exception $e) {
			throw new Exception('API error when retrieving channel with ID `'.H($channel_id).'`: '.H($e->getMessage()).'`');
		}

		if (isset($channels['items'][0])) {
			$channel['name'] = $channels['items'][0]['snippet']['title'] ?? '(unknown)';

			$uploads_id = $channels['items'][0]['contentDetails']['relatedPlaylists']['uploads'] ?? null;

			if (!$uploads_id) {
				throw new Exception('channel with ID `'.H($channel_id).'` appears to have no uploads playlist?');
			}

			$all_videos = api_get_all_videos($uploads_id);

			$channel['numvideos'] = count($all_videos);

			if (isset($all_videos[0])) {
				$channel['earliest'] = str_replace(['T', 'Z'], [' ', ''], $all_videos[0]['snippet']['publishedAt']);
			}

			$videos_page = 1;
			$i = 0;
			$page_videos = [];

			foreach ($all_videos as $video) {
				// add video to page_videos
				$page_videos[$i] = format_video($video);
				$i++;
				
				if ($i === PAGE_NUMVIDEOS) {
					cache_write('videos.' . $channel_id . '.' . $videos_page, $page_videos, CACHE_EXPIRE);
					$videos_page++;
					$i = 0;
					$page_videos = [];
				}
			}

			// write last page if necessary
			if ($page_videos) {
				cache_write('videos.' . $channel_id . '.' . $videos_page, $page_videos, CACHE_EXPIRE);
			}

			$channel['numpages'] = $videos_page;
		} else {
			$channel = false;
		}

		cache_write('channel.' . $channel_id, $channel, CACHE_EXPIRE);
	}

	if ($channel === false) {
		throw new Exception('No channel found that matches ID `'.H($channel_id).'`!');
	}

	return $channel;
}

function api_get_channel2($channel_id) {
	$channel = cache_read('channel.' . $channel_id);

	if (!isset($channel)) {
		// 'create' channel in cache and get all videos from latest to earliest
		$api = api_init();

		try {
			// contentDetails contains uploads
			// snippet contains name, customurl, date created, thumbnails
			// statistics contains number of videos (may be different to uploads if this counts unlisted/private?)
			// note on videoCount: (tested with niilo22, CANNOT BE TRUSTED)
			// 'Note that the value reflects the count of the channel's public videos only, even to owners.' good
			// 'This behavior is consistent with counts shown on the YouTube website.' MAY BE INACCURATE
			$channels = $api->channels->listChannels('contentDetails,snippet', ['id' => $channel_id]);
		} catch (Exception $e) {
			throw new Exception('API error when retrieving channel with ID `'.H($channel_id).'`: '.H($e->getMessage()).'`');
		}

		if (isset($channels['items'][0])) {
			$channel['name'] = $channels['items'][0]['snippet']['title'] ?? '(unknown)';

			$uploads_id = $channels['items'][0]['contentDetails']['relatedPlaylists']['uploads'] ?? null;

			if (!$uploads_id) {
				throw new Exception('channel with ID `'.H($channel_id).'` appears to have no uploads playlist?');
			}

			$channel['uploads_id'] = $uploads_id;

			$get_next_video = function($get_total=false) use ($api, $uploads_id) {
				static $page_token = null;
				static $videos = null;
				static $numvideos = -1;
				static $video_i = null;

				if ($numvideos < 0 || $video_i >= $numvideos) {
					// end of page

					if ($page_token === false) {
						// no more videos and no more pages
						return false;
					}

					// get new page
					$params = ['playlistId' => $uploads_id, 'maxResults' => API_MAXRESULTS];
					if (isset($page_token)) {
						$params['pageToken'] = $page_token;
					}

					try {
						$result = $api->playlistItems->listPlaylistItems('snippet', $params);
					} catch (Exception $e) {
						throw new Exception('API error while retrieving videos for uploads_id `'.H($uploads_id).'`: '.H($e->getMessage()));
					}

					if (isset($result['nextPageToken'])) {
						$page_token = $result['nextPageToken'];
					} else {
						// last page, no more videos
						$page_token = false;
					}

					$videos = $result['items'];
					$numvideos = count($videos);
					$video_i = 0;
				}

				if($get_total) {
					// BODGE
					return $result['pageInfo']['totalResults'];
				}

				$video = $videos[$video_i];

				$video_i += 1;

				return $video;
			};

			$total_videos = $get_next_video(true); // BODGE

			$numpages = (int)ceil($total_videos / PAGE_NUMVIDEOS); // why the fuck does ceil return a float?!
			$page_i = $numpages;
			$remainder = $total_videos % PAGE_NUMVIDEOS;

			// TODO: see if there's a faster way to write an array 'backwards' in PHP

			// write final 'remainder' page first
			if ($remainder) {
				$page_videos = [];

				for ($i = 0; $i < $remainder; $i++) {
					$video = $get_next_video();

					if ($video) {
						$page_videos []= format_video($video);
					} else {
						// should throw exception here!
					}
				}

				cache_write('videos.' . $channel_id . '.' . $page_i, array_reverse($page_videos));

				$page_i -= 1;
			}
			
			// write remaining pages in reverse order (newest to oldest)
			while ($page_i > 0) {
				$page_videos = [];

				for ($i = 0; $i < PAGE_NUMVIDEOS; $i++) {
					$video = $get_next_video();

					if ($video) {
						$page_videos[$i] = format_video($video);

						if ($page_i === 1) {
							// SUPER BODGE!!!!!
							$earliest = $page_videos[$i]['uploaded'];
						}
					} else {
						// should throw exception here!
					}
				}

				cache_write('videos.' . $channel_id . '.' . $page_i, array_reverse($page_videos));

				$page_i -= 1;
			}

			// now, that SHOULD be all videos - but just in case my maths is off
			// and there are any extra videos left over, collect them here.
			$page0_videos = [];

			while ($page0video = $get_next_video()) {
				$page0_videos []= format_video($page0video);
			}

			if ($page0_videos) {
				cache_write('videos.' . $channel_id . '.0', array_reverse($page0_videos));

				$channel['haspage0'] = true;
			}

			$channel['earliest'] = $earliest; // BODGE!!! SEE ABOVE

			$channel['numpages'] = $numpages;
			$channel['numvideos'] = $total_videos;
		} else {
			$channel = false;
		}

		cache_write('channel.' . $channel_id, $channel);
	}

	if ($channel === false) {
		throw new Exception('No channel found that matches ID `'.H($channel_id).'`!');
	}

	return $channel;
}

function format_video($video_data) {
	$video = [];

	$video['id'] = $video_data['snippet']['resourceId']['videoId'];
	$video['title'] = $video_data['snippet']['title'];
	$video['uploaded'] = str_replace(['T', 'Z'], [' ', ''], $video_data['snippet']['publishedAt']);

	foreach (['high', 'medium', 'default'] as $key) if (isset($video_data['snippet']['thumbnails'][$key])) {
		$video['thumb'] = $video_data['snippet']['thumbnails'][$key]['url'];
		$video['width'] = $video_data['snippet']['thumbnails'][$key]['width'];
		$video['height'] = $video_data['snippet']['thumbnails'][$key]['height'];

		break;
	}

	return $video;
}

function api_get_all_videos($uploads_id) {
	$videos_newest = [];

	$page_token = null;

	$api = api_init();

	do {
		$params = ['playlistId' => $uploads_id, 'maxResults' => 50];
		if (isset($page_token)) {
			$params['pageToken'] = $page_token;
		}
		
		try {
			$result = $api->playlistItems->listPlaylistItems('snippet', $params);
		} catch (Exception $e) {
			throw new Exception('API error while retrieving videos for uploads_id `'.H($uploads_id).'`: '.H($e->getMessage()));
		}
		
		foreach ($result['items'] as $item) {
			$videos_newest []= $item;
		}

		if (!empty($result['nextPageToken'])) {
			$page_token = $result['nextPageToken'];
		} else {
			$page_token = null;
			break;
		}
	} while (!empty($result['nextPageToken']));

	return array_reverse($videos_newest);
}

function get_videos_page($channel_id, $page) {
	$videos = cache_read('videos.' . $channel_id . '.' . $page);

	if (!isset($videos)) {
		throw new Exception('no videos found!');
	}

	return $videos;
}

function get_channel_id_by_customurl($customurl) {
	$channel_id = cache_read('customurl.' . $customurl);

	if (!isset($channel_id)) {
		$contents = @file_get_contents('https://www.youtube.com/c/' . $customurl);

		if ($error_get_last = error_get_last()) {
			throw new Exception('failed to retrieve channel ID by customurl: '.H($error_get_last['message']));
		}

		if (preg_match('/UC[a-zA-Z0-9_-]{21}[AQgw]/', $contents, $matches) && isset($matches[0])) {
			$channel_id = $matches[0];
		} else {
			$channel_id = false;
		}

		cache_write('customurl.' . $customurl, $channel_id, CACHE_EXPIRE);
	}

	if ($channel_id === false) {
		throw new Exception('no matching channel ID found for customURL `'.H($customurl).'`!');
	}

	return $channel_id;
}

////////////////////////////////////////////////////////////////////////////////

$error = null;

/*
	retrieve and validate input params
*/

if (!isset($_GET['t']) && !isset($_GET['q'])) {
	$error = '>HOME<'; // BODGE
	$query_key = null;
	$query_value = null;
} else {
	$query_key = null;
	if (isset($_GET['t'])) {
		if ($_GET['t'] === 'channel') {
			$query_key = 'id';
		} elseif ($_GET['t'] === 'user') {
			$query_key = 'forUsername';
		} elseif ($_GET['t'] === 'c') {
			$query_key = 'STUPIDCUSTOMURL';
		}
	}

	if (isset($_GET['q'])) {
		$query_value = $_GET['q'];
	} else {
		$query_value = '';
	}

	if (!$query_key) {
		$error = 'Type of search not defined - please select either channel ID, username or customURL!';
	} elseif (strlen($query_value) < 1 || strlen($query_value) > 30) {
		$error = 'Channel ID or username must be between 1 and 30 characters!';
	} elseif (preg_match('/[^A-Za-z0-9_-]/', $query_value)) {
		$error = 'Channel ID or username must contain only alphanumeric characters, - and _!';
	}
}

// page is 1-indexed
if (isset($_GET['page'])) {
	$page = max(1, (int)$_GET['page']);
} else {
	$page = 1;
}

/*
	if username was specified, get channel id
*/
if (!$error) {
	if ($query_key === 'STUPIDCUSTOMURL') {
		try {
			$channel_id = get_channel_id_by_customurl($query_value);
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	} elseif ($query_key === 'forUsername') {
		try {
			$channel_id = api_get_channel_id_by_username($query_value);
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
	} else {
		$channel_id = $query_value;
	}
}

/*
	get channel's info and videos
*/
if (!$error) try {
	$channel = api_get_channel2($channel_id);
} catch (Exception $e) {
	$error = $e->getMessage();
}

$meta_title = 'SORT BY OLDEST';

if (!$error) {
	$channel_name = H($channel['name']);

	$meta_title = $channel_name . ' | SORT BY OLDEST';

	if ($page <= $channel['numpages']) {
		try {
			$videos = get_videos_page($channel_id, $page);
		} catch (Exception $e) {
			$error = H($e->getMessage());
		}
	} else {
		$error = 'Page number '.$page.' is out of range (maximum page number for this channel is '.$channel['numpages'].')';
	}

	$url = 'https://' . $_SERVER['HTTP_HOST'] . '/?t=' . ['id'=>'channel', 'forUsername'=>'user', 'STUPIDCUSTOMURL'=>'c'][$query_key] . '&q=' . H($query_value);

	$url_page_prev = $url . '&page=' . (min($page, $channel['numpages'])-1);
	$url_page_next = $url . '&page=' . (max($page, 0)+1);

	// pagination (slight bodge)
	$pagination = '';

	if ($page > 1) {
		$pagination .= '<a href="' . $url_page_prev . '">PREV</a> - ';
	}
	if ($page === 1) {
		$pagination .= '<b>1</b> ';
	} else {
		$pagination .= '<a href="' . $url.'&page=1">1</a> ';
	}
	for ($i = 2; $i <= $channel['numpages']; $i++) {
		$pagination .= '&nbsp; ';
		if ($page === $i) {
			$pagination .= '<b>' . $i . '</b> ';
		} else {
			$pagination .= '<a href="' . $url.'&page='.$i . '">' . $i . '</a> ';
		}
	}
	if ($page < $channel['numpages']) {
		$pagination .= ' - <a href="' . $url_page_next . '">NEXT</a>';
	}
}

/*
	get random invidious instance from a list (hardcoded for now lol)
*/
$invidious_instances = [
	'yewtu.be',
	'vid.puffyan.us',
	'inv.riverside.rocks',
	'invidio.xamh.de',
	'y.com.sb',
	'invidious.sethforprivacy.com',
	'yt.artemislena.eu',
	'invidious.tiekoetter.com',
	'invidious.flokinet.to',
	'invidious.nerdvpn.de',
	'inv.bp.projectsegfau.lt',
	'invidious.snopyta.org',
	'inv.odyssey346.dev',
	'invidious.rhyshl.live',
	'invidious.baczek.me',
	'invidious.slipfox.xyz',
	'invidious.esmailelbob.xyz',
	'youtube.076.ne.jp',
	'invidious.dhusch.de',
];
$invidious = $invidious_instances[mt_rand(0, count($invidious_instances)-1)];
$url_watch = 'https://' . $invidious . '/watch?v=';

if ($error === '>HOME<') { // BODGE
	$error = false;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>

<title><?=$meta_title?></title>
<base href="<?=HTTP_SERVER?>" />
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="style.css" />

</head>
<body>

<header class="header">
	<img src="logo.svg" width="32" height="32" class="header__logo" />
	<form action="<?=HTTP_SERVER?>" method="GET" enctype="multipart/form-data" class="searchbar header__searchbar">
		<select name="t" class="searchbar__select">
			<option value="channel"<?=$query_key === 'id'              ? ' selected="selected"' : ''?>>Channel ID - /channel/</option>
			<option value="user"<?=   $query_key === 'forUsername'     ? ' selected="selected"' : ''?>>Username - /user/</option>
			<option value="c"<?=      $query_key === 'STUPIDCUSTOMURL' ? ' selected="selected"' : ''?>>Custom URL - /c/</option>
		</select>
		<input type="text" name="q" value="<?=H($query_value)?>" placeholder="UCq26Z4RAp68ddfCchKqxpXQ" class="searchbar__text" />
		<button type="submit" class="searchbar__submit">TAKE ME BACK</button>
	</form>
</header>
<main class="main">
	<?php if (!empty($error)) { ?>
		<div class="error"><?=$error?></div>
	<?php } ?>
	<?php if (!empty($channel)) { ?>
		<h1><?=$channel['name']?></h1>
		<p>
			<?=$channel['numvideos']?> video(s) since <?=$channel['earliest']?>
			-
			using instance <b><?=$invidious?></b> (refresh for another)
		</p>
		<p class="pagination"><?=$pagination?></p>
		<div class="videos">
			<?php foreach ($videos as $video) { ?>
				<div class="video">
					<a href="<?=$url_watch . $video['id']?>" target="_blank">
						<img src="<?=$video['thumb']?>" alt="<?=$video['title']?>" width="<?=$video['width']?>" height="<?=$video['height']?>" class="video__thumbnail" />
						<span class="video__title"><?=H($video['title'])?></span>
					</a>
					<small class="video__uploaded"><?=$video['uploaded']?></small>
				</div>
			<?php } ?>
		</div>
		<p class="pagination"><?=$pagination?></p>
	<?php } else { ?>
		<p>Look up a youtube channel by one of the following:</p>
		<p>Channel ID - https://youtube.com/channel/<b>UC4QobU6STFB0P71PMvOGN5A</b></p>
		<p>Username - https://youtube.com/user/<b>jawed</b></p>
		<p>Custom URL - https://youtube.com/c/<b>jawed</b></p>
		<p><strong>OR:</strong> change <b>www.youtube.com</b> to <b><?=DOMAIN?></b> in a youtube channel URL (BETA UNTESTED FEATURE)</p>
	<?php } ?>
</main>
<footer class="footer">
	&nbsp;
</footer>

</body>
</html>
