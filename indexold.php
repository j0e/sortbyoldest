<?php
$APIKEY = '<your_api_key_here>';

require_once('vendor/autoload.php');

// header('content-type: text/plain');

//$channel_id_musicalarmageddon = 'UCq26Z4RAp68ddfCchKqxpXQ';
$channel_id_example = '<your_channel_id_here>';

/* get variables */

$key = null;
if (isset($_GET['t'])) {
	if ($_GET['t'] === 'channel') {
		$key = 'id';
	} elseif ($_GET['t'] === 'user') {
		$key = 'forUsername';
	}
}

$value = null;
if (isset($_GET['q'])) {
	$value = $_GET['q'];
}

if (!isset($key) || !isset($value)) {
	die('type or query not defined or invalid');
}

if (preg_match('/[^A-Za-z0-9]/', $value)) {
	die('channel ID or username must contain only alphanumeric characters');
}

/* do it! */
$client = new Google\Client();
$client->setApplicationName('sortbyoldest');
$client->setDeveloperKey($APIKEY);
$api = new Google\Service\YouTube($client);

$channels = $api->channels->listChannels('contentDetails', [$key => $value]);

if (!isset($channels['pageInfo']['totalResults'])) {
	die('something ain\'t right...');
}

if (!$channels['pageInfo']['totalResults']) {
	die('no channel found that matches '.['id'=>'channel id', 'forUsername'=>'user name'][$key].' `'.htmlspecialchars($value).'`');
}

if (!isset($channels['items'][0]['contentDetails']['relatedPlaylists']['uploads'])) {
	die('something else is fucked');
}

if ($key === 'id') {
	$channel_id = $value;
} else {
	$channel_id = $channels['items'][0]['id'];
}

if (is_file(__DIR__.'/cache/'.$channel_id)) {
	$videos = json_decode(file_get_contents(__DIR__.'/cache/'.$channel_id), true);
}

if (empty($videos)) {
	exit('videos is empty');
	$uploads_id = $channels['items'][0]['contentDetails']['relatedPlaylists']['uploads'];

	$videos_reverse = [];

	$pagetoken = null;

	do {
		$params = ['playlistId' => $uploads_id, 'maxResults' => 50];
		if (isset($pagetoken)) {
			$params['pageToken'] = $pagetoken;
		}
		
		$result = $api->playlistItems->listPlaylistItems('snippet', $params);

		foreach ($result['items'] as $item) {
			$videos_reverse []= $item;
		}

		if (!empty($result['nextPageToken'])) {
			$pagetoken = $result['nextPageToken'];
		} else {
			$pagetoken = null;
			break;
		}
	} while (!empty($result['nextPageToken']));

	$videos = array_reverse($videos_reverse); // TODO check if using array_unshift would be faster or slower, lol

	file_put_contents(__DIR__.'/cache/'.$channel_id, json_encode($videos));
} else {
	echo 'using from cache';
}

$url_instance = 'https://www.yewtu.be/';

?>
<!DOCTYPE html>
<html>
<head>
<style>
	* {
		box-sizing: border-box;
	}
	body {
		max-width: 640px;
		margin: 0 auto;
	}
	.videos {
		display: grid;
		grid-template-columns: repeat(4, 25%);
		column-gap: 8px;
	}
	.video {

	}
	.video__thumbnail, .video img {
		max-width: 100%;
		height: auto;
	}
</style>
</head>
<body>
	<div class="videos">
		<?php foreach ($videos as $video) { ?>
			<div class="video">
				<img src="<?=$video['snippet']['thumbnails']['medium']['url']?>" />
				<h3><a href="<?=$url_instance?>watch?v=<?=$video['snippet']['resourceId']['videoId']?>" target="_blank"><?=htmlspecialchars($video['snippet']['title'])?></a></h3>
				<p><small><?=$video['snippet']['publishedAt']?></small></p>
			</div>
		<?php } ?>
	</div>
</body>
</html>

